/*#############################################################################
#                                                                             #
# Script to query if a username exists within Github, and if so, return their #
#           details                                                           #
# Creation Date    - 17/11/2018                                               #
# Author           - Cameron Watt                                             #
# Authors Email    - wanderingfrostwizard@gmail.com                           #
# Tab width        - 2 SPACES                                                 #
#                                                                             # 
#############################################################################*/

// Create a new Request
var request = new XMLHttpRequest();

// Create a re-usable userName variable to store the username string
// Avoids creating it on every call to search()
var username;

// Variable used to enable debugging output
var DEBUG_REQUESTS = false;

// Initially we want to hide the user details section, as it will start empty
hideInfo();

// Control function, which calls each of the following functions
// This is called from the HTML page, when we need to search for a username
function search( )
{
  username = document.getElementById("UserNameEntry").value;

  hideInfo()
  queryUsername( username );
  processRequest();
}

// Searches for a specific username and adds them to the request variable
function queryUsername( username )
{
  // Open a WebRequest to "GET" the users information
  // ASYNCHRONOUSLY to avoid lag or other issues. Set using true
  request.open('GET', "https://api.github.com/users/"+username, true);
  request.send();

  // Wait for a response from the github servers
  request.addEventListener("readystatechange", processRequest, false);
}

// Function to display the information recieved from github.
function processRequest() 
{
  // Check if the request was completed
  if (request.readyState == 4)
  {
    // Username was found
    if ( request.status == 200 )
    {
      if ( DEBUG_REQUESTS == true )
        alert("Successful Request")
      showInfo();

      var response = JSON.parse(request.responseText);
    
      // Display the user information on screen using pre-defined tags
      document.getElementById("Login").innerHTML=response.login;
      document.getElementById("Name").innerHTML=response.name;
      document.getElementById("Type").innerHTML=response.type;
      document.getElementById("Admin").innerHTML=response.site_admin;
      document.getElementById("Company").innerHTML=response.company;
      document.getElementById("Location").innerHTML=response.location;
      document.getElementById("Public_Repos").innerHTML=response.public_repos;
      document.getElementById("Created").innerHTML=response.created_at;
    }
    // Username was NOT found
    /* Hide the user details section, as it is simpler than clearing
       each of the user detail fields */ 
    else
    {
      alert("Username  "+username+"  was NOT found" );
      hideInfo();
    }
  }
}

// Show the user details html block, to display the users info to screen
function showInfo()
{
  document.getElementById("UserDetails").style.display = "block";
}

// Hide the user details html block, to hide the users info on screen
// NOTE this is used, if the user tries to run multiple queries
function hideInfo()
{
  document.getElementById("UserDetails").style.display = "none";
}